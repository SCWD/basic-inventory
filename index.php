<?php include('includes/header.php');?>
<body class="<?php echo basename($_SERVER["SCRIPT_FILENAME"], '.php' );?>">
<?php include('includes/nav.php');?>
<?php include('includes/conn.php');?>


<div class="container mb-3 mt-3">

<table class="table table-striped table-bordered mydatatable" style="width: 100%">
<thead>
    <tr>
        <th>Product ID</th>
        <th>Product Category</th>
        <th>Product Name</th>
        <th>Product Price</th>
        <th>Action</th>
    </tr>
</thead>

    <tbody>

    <?php
    if(mysqli_num_rows($query_run) > 0)
    {
      while($rows = mysqli_fetch_assoc($query_run))
      {

        ?>
    <tr>
    <td class="td20"><?php echo $rows['id'];?></td>
    <td><?php echo $rows['pcat'];?></td>
    <td><?php echo $rows['pname'];?></td>
    <td><?php echo $rows['pprice'];?></td>
    <th>
    <button type="button" class="btn btn-primary btn-block">Edit</button>
    <button type="button" class="btn btn-danger btn-block">Delete</button>
    </th>
    </tr>
    <?php
      }
    }
    ?>


    </tbody>
    
    <tfoot>
    <tr>
        <th>Product ID</th>
        <th>Product Category</th>
        <th>Product Name</th>
        <th>Product Price</th>
        <th>Action</th>
    </tr>
    </tfoot>

</table><hr>

<form action="multi_function.php" method="POST">
  <fieldset>
    <div class="form-group">
      <label for="pcat">CATEGORY</label>
      <select class="form-control" id="pcat" name="pcat">
        <option>Groceries</option>
        <option>LPG</option>
        <option>Others</option>
      </select>
    </div>

    <div class="form-group">
      <label for="pname">NAME</label>
      <input type="text" class="form-control" name="pname" id="pname" placeholder="Product Name">
    </div>

    <div class="form-group">
      <label for="pprice">PRICE</label>
      <input type="number" class="form-control" name="pprice" id="pprice" placeholder="Product Price">
    </div>
    
    <button type="submit" name="addprodbtn" class="btn btn-primary">Add Product</button>


   </fieldset>
</form>

</div>





<?php include('includes/footer.php'); ?>